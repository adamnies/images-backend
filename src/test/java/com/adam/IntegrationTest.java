package com.adam;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class IntegrationTest {

    @Autowired
    ApplicationContext context;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void contextLoads() throws Exception {

    }

    @Before
    public void setUp() {
    }


    @Test
    public void shouldApplyDefaultPageable() throws Exception {

        mockMvc.perform(get("/books/default-pageable"))//
                .andExpect(jsonPath("$.content[0].sales").value(0)) //
                .andExpect(jsonPath("$.size").value(1));
    }

    @Test
    public void shouldOverrideDefaultPageable() throws Exception {

        mockMvc.perform(get("/books/default-pageable?size=10"))//
                .andExpect(jsonPath("$.content[0].sales").value(0)) //
                .andExpect(jsonPath("$.size").value(10));
    }
}