package com.adam.bootstrap;

import com.adam.config.ImageRecognizer;
import com.adam.model.*;
import com.adam.repository.AuthorityRepository;
import com.adam.repository.PostRepository;
import com.adam.repository.SubscriptionRepository;
import com.adam.repository.UserRepository;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Log4j
@Component
public class RoleBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthorityRepository authorityRepository;

    @Autowired
    PostRepository postRepository;

    @Autowired
    SubscriptionRepository subscriptionRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        authorityRepository.save(this.getAuthorityList());
        userRepository.save(this.getUserList());
        postRepository.save(this.getPostList());
        subscriptionRepository.save(this.getSubscriptionList());
    }

    private List<Authority> getAuthorityList() {
        List<Authority> authorityList = new ArrayList<>();
        Authority user = new Authority();
        user.setName(AuthorityName.ROLE_USER);

        Authority admin = new Authority();
        admin.setName(AuthorityName.ROLE_ADMIN);
        authorityList.add(user);
        authorityList.add(admin);
        return authorityList;
    }

    private List<User> getUserList() {
        List<User> userList = new ArrayList<>();
        User user = new User();
        user.setEmail("adamek@gmail.com");
        user.setUsername("user");
        user.setPassword("user");
        user.setImageUrl("https://files.cults3d.com/uploaders/5871147/illustration-file/1470305062-19497-5911/pikachu_large.jpg");
        user.setAbout("Opis osoby profil super web security user repository inaczej tak");
        user.setCity("Katowice");
        user.setFirstname("Adam");
        user.setLastname("Kowalski");

        List<Authority> authorityList = new ArrayList<>();
        Authority authority = authorityRepository.findOne(1L);
        authorityList.add(authority);
        user.setAuthorities(authorityList);

        User user1 = new User();
        user1.setUsername("admin");
        user1.setPassword("admin");
        user1.setFirstname("Adam");
        user1.setImageUrl("https://cdn.shopify.com/s/files/1/0225/1115/products/environments-cartoon-low-poly-forest-island-2-0-anton-moek-1_2000x.jpg?v=1513926370");
        user1.setCity("sosnowiec");
        user1.setAbout("Opis osoby profil super web security user repository inaczej tak");
        user1.setLastname("Niestoj");
        user1.setEmail("adam@gmail.com");

        List<Authority> authorityList1 = new ArrayList<>();
        Authority authority1 = authorityRepository.findOne(2L);
        authorityList1.add(authority1);
        user1.setAuthorities(authorityList1);

        userList.add(user);
        userList.add(user1);
        return userList;
    }

    private List<Post> getPostList() {
        List<Post> postList = new ArrayList<>();
        Post post = new Post();
        post.setDescription("Post co tutaj napisać fjanego post super tak");
        post.setAuthor(userRepository.getOne(1L));
        post.setOwner(userRepository.getOne(1L));
        postList.add(post);
        return postList;
    }

    private List<Subscription> getSubscriptionList() {
        List<Subscription> subscriptionList = new ArrayList<>();
        Subscription subscription = new Subscription();
        subscription.setSubscriber(userRepository.getOne(1L));
        subscription.setSubscribed(userRepository.getOne(2L));
        subscriptionList.add(subscription);
        return subscriptionList;
    }

}
