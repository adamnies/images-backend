package com.adam.repository;

import com.adam.model.Media;
import com.adam.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface MediaRepository extends JpaRepository<Media, Long> {

    Media findMediaByUrl(String url);

}
