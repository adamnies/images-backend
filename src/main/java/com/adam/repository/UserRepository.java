package com.adam.repository;

import com.adam.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

@RepositoryRestResource
public interface UserRepository extends JpaRepository<User, Long> {

    @PreAuthorize("#username == principal.username")
    List<User> findUsersByUsername(@Param("username") String username);

    User findUserByUsername(@Param("username") String username);
}
