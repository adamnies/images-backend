package com.adam.repository;

import com.adam.config.ImageRecognizer;
import com.adam.model.Category;
import com.adam.model.CategoryRatio;
import com.adam.model.Media;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.data.rest.core.event.AbstractRepositoryEventListener;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@RepositoryEventHandler(Media.class)
public class MediaEventHandler extends AbstractRepositoryEventListener {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    MediaRepository mediaRepository;

    @Autowired
    CategoryRatioRepository categoryRatioRepository;

    @HandleAfterCreate
    public void handleAfterCreate(Media media) {
        List<String> resultList = ImageRecognizer.recognize(media.getUrl());

        for (String result : resultList) {
            if (categoryRepository.findCategoryByName(result) == null) {
                Category category = new Category();
                category.setName(result);
                categoryRepository.save(category);
            }

            CategoryRatio categoryRatio = new CategoryRatio();
            categoryRatio.setMedia(media);
            Category category = categoryRepository.findCategoryByName(result);
            categoryRatio.setCategory(category);
            Double aDouble = 1.0;
            categoryRatio.setValue(aDouble);
            categoryRatioRepository.save(categoryRatio);
        }


    }
}