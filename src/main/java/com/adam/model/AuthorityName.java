package com.adam.model;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}