package com.adam.model;

import org.springframework.data.rest.core.config.Projection;

import java.util.Date;
import java.util.List;

@Projection(name = "media", types = Media.class)
public interface MediaProjection {
    String getUrl();
    String getType();
    String getActive();
    Date getCreatedAt();
    List<CategoryRatioProjection> getCategoryRatios();
}