package com.adam.model;

import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

@Projection(name = "subscribed", types = Subscription.class)
public interface SubscriptionProjection {
    Date getCreatedAt();
    User getSubscribed();
    User getSubscriber();

}