package com.adam.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Data
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    @NotNull
    private String username;

    @NotNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Column(unique = true)
    @NotNull
    private String email;
    private String firstname;
    private String lastname;
    private String imageUrl = "https://i.pinimg.com/736x/4e/4e/7b/4e4e7be06c1a3836c0b20772be2a7875--avatar-pixel.jpg";
    private String about;
    private String city;

    @NotNull
    private Boolean enabled = true;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastPasswordResetDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_authority", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "authority_id", referencedColumnName = "id")})
    private List<Authority> authorities;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
    private Collection<Post> ownerPostList;
    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    private Collection<Post> authorPostList;

    @OneToMany(mappedBy = "subscriber", cascade = CascadeType.ALL)
    private Collection<Subscription> subscriberList;
    @OneToMany(mappedBy = "subscribed", cascade = CascadeType.ALL)
    private Collection<Subscription> subscribedList;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
    private Collection<Media> mediaList;

    public void setPassword(String password) {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        this.password = encoder.encode(password);
    }
}
