package com.adam.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class CategoryRatio {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Double value;

    @ManyToOne
    @JoinColumn(name = "media_id")
    private Media media;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
}
