package com.adam.model;

import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "categoryRatio", types = CategoryRatio.class)
public interface CategoryRatioProjection {
    Double getValue();
    Category getCategory();
}