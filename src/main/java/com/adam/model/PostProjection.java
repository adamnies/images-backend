package com.adam.model;

import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

@Projection(name = "author", types = Post.class)
public interface PostProjection {
    Date getCreatedAt();
    String getDescription();
    User getAuthor();
}