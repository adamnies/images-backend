package com.adam.model;

import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "role", types = User.class)
public interface UserProjection {
    String getUsername();
    String getEmail();
    String getFirstname();
    String getLastname();
    String getEnabled();
    String getImageUrl();
    String getAbout();
    String getCity();
    List<Authority> getAuthorities();
}