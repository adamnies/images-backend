package com.adam.controller;

import com.adam.model.Media;
import com.adam.repository.MediaRepository;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@Log4j
public class MediaController {
    @Autowired
    MediaRepository mediaRepository;

    @RequestMapping(value = "/user/", method = RequestMethod.POST)
    public ResponseEntity<?> createUser(@RequestBody Media media, UriComponentsBuilder ucBuilder) {
        Media media1 = new Media();
        mediaRepository.save(media1);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/users/{id}").buildAndExpand(media.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

}